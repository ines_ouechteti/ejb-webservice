package webSERVICE;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import charity.Entity.actes;
import charity.service.affichageActeRemote;

@Path("affichage")
public class affichage {

	@EJB
	private affichageActeRemote Affichage;

	// affichage tous les actes
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("aff")
	@GET
	public List<actes> affichage1() {
		return Affichage.findAllActe();
	}

	@Produces({ MediaType.APPLICATION_XML })
	@Path("json1")
	@GET
	public List<actes> affichage2() {
		return Affichage.findAllActe();
	}

	// affichage par reference
	@Path("{reference}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public actes affichageCIN(@PathParam("reference") String reference) {
		return Affichage.findCandidatByREFERENCE(reference);
	}

}
