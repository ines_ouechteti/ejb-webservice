package webSERVICE;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import charity.Entity.actes;
import charity.Entity.admin;
import charity.Entity.contact;
import charity.Entity.utilisateur;
import charity.service.GestionProfilRemote;

@Path("compte")
public class AjouterCompte {

	@EJB
	GestionProfilRemote gestionProfilRemote;
	
	// ajout admin

			@POST
			@Consumes(MediaType.APPLICATION_JSON)
		    @Path("C")
			public Response AjoutActes(admin admin) {
				gestionProfilRemote.addAdmin(admin);
				return Response.status(Status.ACCEPTED).entity("admin a �t� ajout�e avec succ�s ").build();
			}

			
			// affichage par cin
			@Path("{cin}")
			@GET
			@Produces({ MediaType.APPLICATION_JSON })
			public utilisateur affichageCIN(@PathParam("cin") int cin) {
				return gestionProfilRemote.findCandidatCin(cin);
			}
			
			
			// modifier admin
			@PUT
			@Path("Update")
			@Consumes(MediaType.APPLICATION_JSON)
			public Response modifUser(admin e) {
				gestionProfilRemote.updateUtilisateur(e);
				return Response.status(Status.OK).entity("update succesfull").build();
			}	
			
			// modifier actes
			@PUT
			@Path("U")
			@Consumes(MediaType.APPLICATION_JSON)
			public Response modifActe(actes actes) {
				gestionProfilRemote.updateActes(actes);
				return Response.status(Status.OK).entity("update succesfull").build();
			}	
			
			// ajouter contact
			@POST
			@Consumes(MediaType.APPLICATION_JSON)
			@Path("A")
			public Response AjoutContact(contact contact) {
				gestionProfilRemote.addContact(contact);
				return Response.status(Status.ACCEPTED).entity("contact a �t� ajout�e avec succ�s ").build();
			}
			
			}

