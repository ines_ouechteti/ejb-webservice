package webSERVICE;

import javax.ejb.EJB;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import charity.Entity.utilisateur;
import charity.service.GestionProfilRemote;



@Path("login")
public class authentif {

	@EJB
	GestionProfilRemote gestionProfilRemote;
	
	
	
		@GET
		@Path("l")
		@Produces(MediaType.TEXT_PLAIN)
		public utilisateur login1(
				@QueryParam(value = "login") String login,
				@QueryParam(value = "password") String password) {
			return gestionProfilRemote.login(login,password);
		}
		//authentification path param
		@GET
		@Path(value = "authentif1/{email}/{password}")
		@Produces(MediaType.TEXT_PLAIN)
		public utilisateur login(
				@PathParam(value = "email") String login,
				@PathParam(value = "password") String password) {
			return gestionProfilRemote.login(login, password);
		}
		
		//authentification  defaultValue 
		@GET
		@Path(value = "authentif")
		@Produces(MediaType.TEXT_PLAIN)
		public utilisateur login3(
				@DefaultValue("all") @HeaderParam(value = "email") String login,
				@DefaultValue("??????") @HeaderParam(value = "password") String password
				) {
			return gestionProfilRemote.login(login, password);
		
		}
		
	}

