package charity.Entity;

import java.io.Serializable;

import javax.persistence.Entity;

import javax.persistence.Id;



@Entity
public class contact  implements Serializable {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	
	private String emailP ;
	private String emailS ;
	private String message ;
	
	public contact() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	public contact(String emailP, String emailS, String message) {
		super();
		this.emailP = emailP;
		this.emailS = emailS;
		this.message = message;
	}



	public void setEmailP(String emailP) {
		this.emailP = emailP;
	}
	public String getEmailP() {
		return emailP;
	}
	public String getEmailS() {
		return emailS;
	}
	public String getMessage() {
		return message;
	}
	public void setEmailS(String emailS) {
		this.emailS = emailS;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
