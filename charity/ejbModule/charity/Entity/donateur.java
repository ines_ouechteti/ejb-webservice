package charity.Entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;



@Entity
public class donateur extends utilisateur {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="donateur", fetch=FetchType.EAGER)
	private List<actes> actes ;
	public List<actes> getActes() {
		return actes;
	}
	
	public void setActes(List<actes> actes) {
		this.actes = actes;
	}
	
	
	
	
	public donateur() {
		// TODO Auto-generated constructor stub
	}
	
	
}
