package charity.service;

import java.util.List;

import javax.ejb.Local;

import charity.Entity.actes;


@Local
public interface affichageActeLocal {

	actes findCandidatByREFERENCE(String reference);

	List<actes> findAllActe();

	void deleteActes(actes actes);

	



}
