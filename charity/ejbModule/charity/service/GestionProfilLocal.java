package charity.service;



import javax.ejb.Local;

import charity.Entity.actes;
import charity.Entity.contact;
import charity.Entity.utilisateur;


@Local
public interface GestionProfilLocal {

	void addActe(actes actes);

	void addAdmin(utilisateur utilisateur);

	utilisateur login(String email, String password);

	void updateUtilisateur(utilisateur utilisateur);

	void updateActes(actes actes);

	void addContact(contact contact);

	utilisateur findCandidatCin(int cin );



	
}
