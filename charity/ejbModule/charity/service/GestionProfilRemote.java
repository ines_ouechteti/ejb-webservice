package charity.service;

import javax.ejb.Remote;

import charity.Entity.actes;
import charity.Entity.contact;
import charity.Entity.utilisateur;

@Remote
public interface GestionProfilRemote {

	void addActe(actes actes);

	void addAdmin(utilisateur utilisateur);

	utilisateur login(String email, String password);

	void updateUtilisateur(utilisateur utilisateur);
	
	void updateActes(actes actes);
	
	void addContact(contact contact);
	utilisateur findCandidatCin(int cin);

}
