package charity.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import charity.Entity.actes;




@Stateless
@LocalBean
public class affichageActe implements affichageActeLocal , affichageActeRemote {
	

		
			@PersistenceContext(unitName="charity")
			private EntityManager entityManager;
		
			@Override
			public List<actes> findAllActe()
			{
					Query et = entityManager.createQuery("From actes");
						return et.getResultList();									
			}
			
			
		
			@Override
		public actes findCandidatByREFERENCE( String reference) {
			return entityManager.find(actes.class,reference);}
			
			
			// delete acte
			@Override
			public void deleteActes(actes actes)
			{
				entityManager.remove(entityManager.merge(actes));
			}
			
			
			

		}

