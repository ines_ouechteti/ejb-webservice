package charity.service;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import charity.Entity.actes;
import charity.Entity.contact;
import charity.Entity.utilisateur;


@Stateless
public class GestionProfil implements GestionProfilLocal , GestionProfilRemote {

	
	
		@PersistenceContext(unitName = "charity")
		private EntityManager entityManager;

		
		//authentif 
		@Override
		public utilisateur login( String email, String password) {
			try {
			String myFirstJPQLQuery="SELECT e FROM utilisateur e WHERE e.email=:p1 AND e.password=:p2";
			Query query = entityManager.createQuery(myFirstJPQLQuery);
			query.setParameter("p1",email);
			query.setParameter("p2",password);
			return(utilisateur) query.getSingleResult();}
			catch (Exception e)
			{
				return null;
			}
			
		}
		
		// add contact 
		@Override
		public void addContact(contact contact) {
			entityManager.persist(contact);
		}
		

		// les cruds d'adminstrateur
		@Override
		public void addAdmin(utilisateur utilisateur) {
			entityManager.persist(utilisateur);
		}

		// update utulisateur 
		@Override
		public void updateUtilisateur(utilisateur utilisateur) {
			
			entityManager.merge(utilisateur);
		}
		
		
		   // ajouter un actes
				@Override
				public void addActe(actes actes) {
					entityManager.persist(actes);}

		
		// update acte 
		@Override
		public void updateActes(actes actes ) {
			
			entityManager.merge(actes);
		}

		@Override
		public utilisateur findCandidatCin( int cin) {
			return entityManager.find(utilisateur.class,cin);}
		

		
}



		

	

