package charity.service;

import java.util.List;

import javax.ejb.Remote;

import charity.Entity.actes;

@Remote
public interface affichageActeRemote {

	actes findCandidatByREFERENCE(String reference);

	List<actes> findAllActe();
	void deleteActes(actes actes);

}
